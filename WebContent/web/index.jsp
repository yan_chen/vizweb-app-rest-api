<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
<title>YC</title>
<script src="../common/jquery/jquery.min.js" type="text/javascript"></script>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<link
	href="http://code.google.com//apis/maps/documentation/javascript/examples/default.css"
	rel="stylesheet" type="text/css">
	<link href="css/default.css" rel="stylesheet" type="text/css" />

	<script type="text/javascript" src="../common/swfupload/swfupload.js"></script>
	<script type="text/javascript" src="js/markerColor.js"></script>
	<script type="text/javascript" src="js/inforbox.js"></script>
	<script type="text/javascript"
		src="../common/swfupload/swfupload.queue.js"></script>
	<script type="text/javascript" src="js/fileprogress.js"></script>
	<script type="text/javascript" src="js/countryName.js"></script>
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>

	<!-- Google Map -->
	<script type='text/javascript'>
    var map;
    var map1;
    var markers = [];
    function initialize() {
	  var mapOptions = {
	    zoom: 1,
	    center: new google.maps.LatLng(0, 0),
	    mapTypeControl: true,
	    mapTypeControlOptions: {
	      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
	    },
	    zoomControl: true,
	    zoomControlOptions: {
	      style: google.maps.ZoomControlStyle.SMALL
	    },
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  }
	  map = new google.maps.Map(document.getElementById('map-canvas'),
	                                mapOptions);
	
	}  
    function clearOverlays() {
    	  setAllMap(null);
    	}
    function deleteOverlays() {
    	  clearOverlays();
    	  markers = [];
    	}
    function setAllMap(map) {
    	  for (var i = 0; i < markers.length; i++) {
    	    markers[i].setMap(map);
    	  }
    	}
   	var styleMarker1;
    var styleMarker3;
    var styleMarker2;
    var ib; 
   	
   	function addMarker(country1, country2,country3, appeal1, appeal2, appeal3) {
   		initialize();
   		var top = countryName(country1);
   		var medium = countryName(country2);
   		var bottom = countryName(country3);
      
      var styleIconClass1 = new StyledIcon(StyledIconTypes.CLASS,{color:"#0B6121"});
      var styleMarker1 = new StyledMarker(
    	    	{
    	    		styleIcon: new StyledIcon(StyledIconTypes.MARKER,{text:"1"},styleIconClass1),
    	    		position:top,
    	    		map:map

    	    	});
      var styleIconClass2 = new StyledIcon(StyledIconTypes.CLASS,{color:"#01DF3A"});
	  styleMarker2 = new StyledMarker(
    	    	{
    	    		styleIcon: new StyledIcon(StyledIconTypes.MARKER,{text:"2"},styleIconClass2),
    	    		position:medium,
    	    		map:map

    	    	});
      var styleIconClass3 = new StyledIcon(StyledIconTypes.CLASS,{color:"#A9F5BC"});
       styleMarker3 = new StyledMarker(
    	    	{
    	    		styleIcon: new StyledIcon(StyledIconTypes.MARKER,{text:"3"},styleIconClass3),
    	    		position:bottom,
    	    		map:map

    	    	}); 	   
       
      	
	    var boxText = document.createElement("div");
		boxText.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
		boxText.innerHTML = country1+"<br>"+"appeal: "+appeal1+"<br>";
		var myOptions1 = {
				 content: boxText
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-140, 0)
				,zIndex: null
				,boxStyle: { 
				  background: "url('tipbox.gif') no-repeat"
				  ,opacity: 0.75
				  ,width: "280px"
				 }
				,closeBoxMargin: "10px 2px 2px 2px"
				,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};
		
		var boxText1 = document.createElement("div");
		boxText1.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
		boxText1.innerHTML = country2+"<br>"+"appeal: "+appeal2+"<br>";
		var myOptions2 = {
				 content: boxText1
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-140, 0)
				,zIndex: null
				,boxStyle: { 
				  background: "url('tipbox.gif') no-repeat"
				  ,opacity: 0.75
				  ,width: "280px"
				 }
				,closeBoxMargin: "10px 2px 2px 2px"
				,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};
		
		var boxText2 = document.createElement("div");
		boxText2.style.cssText = "border: 1px solid black; margin-top: 8px; background: yellow; padding: 5px;";
		boxText2.innerHTML = country3+"<br>"+"appeal: "+appeal3+"<br>";
		var myOptions3 = {
				 content: boxText2
				,disableAutoPan: false
				,maxWidth: 0
				,pixelOffset: new google.maps.Size(-140, 0)
				,zIndex: null
				,boxStyle: { 
				  background: "url('tipbox.gif') no-repeat"
				  ,opacity: 0.75
				  ,width: "280px"
				 }
				,closeBoxMargin: "10px 2px 2px 2px"
				,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
				,infoBoxClearance: new google.maps.Size(1, 1)
				,isHidden: false
				,pane: "floatPane"
				,enableEventPropagation: false
			};
	   	ib = new InfoBox();
	   
	   	google.maps.event.addListener(styleMarker3, "click", function (e) {					
	   	
	   		ib.close();
	   		ib.setOptions(myOptions3);
	   		ib.open(map, styleMarker3);	
	   		
		});
	   	
	   	google.maps.event.addListener(styleMarker2, "click", function (e) {					
		   	
	   		ib.close();
	   		ib.setOptions(myOptions2);
	   		ib.open(map, styleMarker2);	
	   		
		});
		
	   	google.maps.event.addListener(styleMarker1, "click", function (e) {					
		   	
	   		ib.close();
	   		ib.setOptions(myOptions1);
	   		ib.open(map, styleMarker1);	
	   		
		});
	}
    
   	google.maps.event.addDomListener(window, 'load', initialize);
   		
    </script>

	<style type="text/css">
	ul.images {
		margin-left: 8px;
		margin-top: 1px;
		margin-bottom: 1px;
		padding: 1px;
		white-space: nowrap;
		border: 1px solid white;
		width: 1050px;
		height: 129px;
		overflow-x: auto;
		background-color: #FFFFF;
	}
	
	ul.images li {
		float: center;
		display: inline;
		width: auto;
		height: auto;
	}
	</style>

	<!-- jQuery -->
	<script type="text/javascript">	
	
	$(document).ready(function(){
		$("#thumbnails img").live({						
			click:function(e){
				
			 	var country1 = ($(this).attr("topone"));
				var country2 = ($(this).attr("toptwo"));
				var country3 = ($(this).attr("topthree"));
				var appeal1  = ($(this).attr("appealone"));
				var appeal2  = ($(this).attr("appealtwo"));
				var appeal3  = ($(this).attr("appealthree"));  			
				
				$("#test").attr("value",country1);
		 	   addMarker(country1,country2,country3,appeal1,appeal2,appeal3); 	 		 
		},
		   mouseover:function(e){
				$(this).css("border","1px solid orange");
		},
			mouseout:function(e){
				$(this).css("border","1px solid white");
		}
		
		});
	});
	</script>
	<script type="text/javascript">	
	
	$(document).ready(function(){
		$('#updateUsername').submit(function(){			
			$.ajax({
				url:'update',
				type: 'POST',
				dataType:'json',
				data:$('#updateUsername').serialize(),
				success:function(data){
					if(data.isValid){
						alert("Please enter valid");
						$('#displayName').html('your name '+data.username);
						$('#displayName').slideDown(500);
					}
					else{
						alert("Please enter valid");
					}
				},
				error:function(){
					alert("what ");
					updateServerStep(23);
				}
			});
			return false;
		});
	});
	</script>

	</script>

	<script type="text/javascript">
		// implement JSON.stringify serialization
		JSON.stringify = JSON.stringify || function (obj) {
		    var t = typeof (obj);
		    if (t != "object" || obj === null) {
		        // simple data type
		        if (t == "string") obj = '"'+obj+'"';
		        return String(obj);
		    }
		    else {
		        // recurse array or object
		        var n, v, json = [], arr = (obj && obj.constructor == Array);
		        for (n in obj) {
		            v = obj[n]; t = typeof(v);
		            if (t == "string") v = '"'+v+'"';
		            else if (t == "object" && v !== null) v = JSON.stringify(v);
		            json.push((arr ? "" : '"' + n + '":') + String(v));
		        }
		        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
		    }
		};
		var swfu;
		window.onload = function() {
			
			var settings = {
				flash_url : "../common/swfupload/swfupload.swf",
				flash9_url : "../common/swfupload/swfupload_fp9.swf",
				upload_url: "../service/upload",
				file_post_name : "file",
				post_params: {"sessionID" : "<%=session.getId()%>"
				},
				file_size_limit : "5 MB",
				file_types : "*.jpeg;*.jpg;*.png",
				file_types_description : "image",
				file_upload_limit : 0,
				file_queue_limit : 0,
				custom_settings : {
					progressTarget : "fsUploadProgress"
				},
				debug : false,

				// Button settings
				button_image_url : "images/XPButtonUploadText_61x22.png",
				button_placeholder_id : "spanButtonPlaceHolder",
				button_width : 61,

				button_height : 22,
				button_text_left_padding : 18,
				button_window_mode : SWFUpload.WINDOW_MODE.TRANSPARENT,
				button_cursor : SWFUpload.CURSOR.HAND,

				// The event handler functions are defined in handlers.js
				swfupload_preload_handler : preLoad,
				swfupload_load_failed_handler : loadFailed,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete
			};

			swfu = new SWFUpload(settings);

		};
		function fileSetting() {
			var hasimage = $('#thumbnails').attr("name");
			var url = hasimage;
			alert(url);
			$.getJSON('../service/capture?url=' + url, function(data) {
				if (data.success) {
					alert("yes");
				} else {
					alert(data.msg);
				}
			});
			return;
		}
		$(document).ready(function() {

			var hasimage = $('#thumbnails').attr("name");
			var url = hasimage;

			$.getJSON('../service/capture?url=' + url, function(data) {
				if (data.success) {
					alert("yes222222222");
				} else {
					alert(data.msg);
				}
			});
		});
		function postUrl() {
			var hasimage = $('#thumbnails').attr("name");
			var url = $('#weburl').val() + hasimage;
			if (!url) {
				aler("www.google.com");
			} else {
				$('#postBtn').attr('disabled', "disabled");
				$('#postBtn').val("sending...");
				$.getJSON('../service/capture?url=' + url, function(data) {
					if (data.success) {
						$("#download").attr("href", data.csvPath);
						addImage(data.imagePath, data.topOne, data.appealOne,
								data.topTwo, data.appealTwo, data.topThree,
								data.appealThree);
						addMarker(data.topOne, data.topTwo, data.topThree,
								data.appealOne, data.appealTwo,
								data.appealThree);
					} else {
						alert(data.msg);
					}
					$('#postBtn').removeAttr("disabled");
					$('#postBtn').val("send the image");
				});
			}
		}
		//set server
	</script>
	<script>
		
	</script>
</head>

<body>

	<div id="im">
		<a href="www.vizweb.org"> <img src="images/vizweb.jpg" width="240"
			height="90"></a>
	</div>


	<div id="block">
		<table>
			<tr>
				<td width="280px" height="20px">
					<h3>
						<span font-weight="bold">Upload Your Image</span>
					</h3>
				</td>

				<td width="80px"></td>

				<td>
					<h3>
						<span font-weight="bold">Enter An URL</span>
					</h3>
				</td>
			</tr>


			<tr>
				<td><span id="spanButtonPlaceHolder"></span></td>

				<td id="or">
					<h3>Or</h3>
				</td>

				<td>
					<div id="left">
						<input type="text" name="url" id="weburl" size="35"
							font-size:"16" placeholder="www.domain.com" /> <input
							id="postBtn" type="button" value="send the image"
							onclick="postUrl();" />
					</div>
				</td>
			</tr>

			<tr>
				<td><span class="flash" id="fsUploadProgress"></span></td>

				<td></td>

				<td>
					<div id="left"></div>
				</td>
			</tr>
		</table>
	</div>


	<div id="inside">
		<ul class="images">
			<li>
				<div id="thumbnails" name="0"></div>
			</li>
		</ul>
	</div>

	<a id="download">click here to download csv</a>

	<table>
		<tr>
			<td>
				<div id="block2">
					<div id="map-canvas" style="width: 500px; height: 380px;"></div>
				</div>
			</td>

			<!--   <td>
	   		<div id="block2">
				<div id="map-canvas1" style="width: 500px; height: 380px;"></div>
			</div>
		 </td> -->
		</tr>
	</table>

	<script type="text/javascript" src="js/handlers.js"></script>
</body>
</html>
