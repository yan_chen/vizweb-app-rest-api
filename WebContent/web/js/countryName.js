/*************************************************************
 * countryName.js
 * 
 * The coordinates of all 49 countries, they are used by 
 * GoogleMap API
 * 
 * Author: Yan Chen - August 20th, 2013
 *
 * © Copyright 2013 Yan Chen
 * For questions about this file and permission to use
 * the code, contact me at yanchen@seas.harvard.edu
 *************************************************************/

function countryName(name){
	if(name=='Argentina')
	{
		var Argentina = new google.maps.LatLng(-36.315125,-65.593872);
		return Argentina;
	}
	if(name=='Australia')
	{
	 	var Australia = new google.maps.LatLng(-25.005972656239177, 132.92633056640625);
	 	return Australia;
	}
	if(name=='Austria')
	{
	 	var Austria = new google.maps.LatLng(47.279229002570816, 14.335098266601562);
	 	return Austria;
	}
	if(name=='Belgium')
	{
	 	var Belgium= new google.maps.LatLng(50.40151532278236, 4.939727783203125);
	 	return Belgium;
	}
	if(name=='Bosnia And Herzegovina')
	{
	 	var Bosnia_And_Herzegovina= new google.maps.LatLng(44.174325,17.840939);
	 	return Bosnia_And_Herzegovina;
	}
	if(name=='Brazil')
	{
	 	var Brazil= new google.maps.LatLng(-11.695273,-50.111389);
	 	return Brazil;
	}
	if(name=='Bulgaria')
	{
	 	var Bulgaria= new google.maps.LatLng(41.57436130598913, 22.429962158203125);
	 	return Bulgaria;
	}
	if(name=='Canada')
	{
	 	var Canada= new google.maps.LatLng(59.355596,-106.02356);
	 	return Canada;
	}
	if(name=='Chile')
	{
	 	var Chile= new google.maps.LatLng(-30.297018,-70.515747);
	 	return Chile;
	}
	if(name=='China')
	{
	 	var China = new google.maps.LatLng(34.524661,102.203064);
	 	return China;
	}
	if(name=='Colombia')
	{
	 	var Colombia= new google.maps.LatLng(4.346411,-73.483429);
	 	return Colombia;
	}
	if(name=='Croatia')
	{
	 	var Croatia= new google.maps.LatLng(45.490946,15.750961);
	 	return Croatia;
	}
	if(name=='Czech')
	{
	 	var Czech= new google.maps.LatLng(49.823809,15.324211);
	 	return Czech;
	}
	if(name=='Denmark')
	{
	 	var Denmark = new google.maps.LatLng(56.157788,9.576645);
	 	return Denmark ;
	}
	if(name=='Finland')
	{
	 	var Finland= new google.maps.LatLng(65.127638,26.93161);
	 	return Finland;
	}
	if(name=='France')
	{
	 	var France= new google.maps.LatLng(47.457809,2.146454);
	 	return France;
	}
	if(name=='Germany')
	{
	 	var Germany= new google.maps.LatLng(50.764259,10.232391);
	 	return Germany;
	}
	if(name=='Greece')
	{
	 	var Greece= new google.maps.LatLng(39.232253,21.695251);
	 	return Greece;
	}
	if(name=='Hungary')
	{
	 	var Hungary= new google.maps.LatLng(47.100045,19.065399);
	 	return Hungary;
	}
	if(name=='India')
	{
	 	var India= new google.maps.LatLng(23.079732,79.17572);
	 	return India;
	}
	if(name=='Indonesia')
	{
	 	var Indonesia= new google.maps.LatLng(-0.615223,112.574158);
	 	return Indonesia;
	}
	if(name=='Iran')
	{
	 	var Iran= new google.maps.LatLng(32.731841,53.518524);
	 	return Iran;
	}
	if(name=='Ireland')
	{
	 	var Ireland= new google.maps.LatLng(53.291489,-7.891617);
	 	return Ireland;
	}
	if(name=='Israel')
	{
	 	var Israel= new google.maps.LatLng(30.883369,34.836788);
	 	return Israel;
	}
	if(name=='Italy')
	{
	 	var Italy= new google.maps.LatLng(43.004647,11.638641);
	 	return Italy;
	}
	if(name=='Japan')
	{
	 	var Japan= new google.maps.LatLng(36.456636,138.677673);
	 	return Japan;
	}
	if(name=='Lithuania')
	{
	 	var Lithuania= new google.maps.LatLng(55.472627,23.640862);
	 	return Lithuania;
	}
	if(name=='Macedonia')
	{
	 	var Macedonia= new google.maps.LatLng(41.578471,21.4884);
	 	return Macedonia;
	}
	if(name=='Malaysia')
	{
	 	var Malaysia= new google.maps.LatLng(4.182073,101.984367);
	 	return Malaysia;
	}
	if(name=='Mexico')
	{
	 	var Mexico= new google.maps.LatLng(23.725012,-103.981476);
	 	return Mexico;
	}
	if(name=='Netherlands')
	{
	 	var Netherlands= new google.maps.LatLng(52.126744,5.425529);
	 	return Netherlands;
	}
	if(name=='New Zealand')
	{
	 	var New_Zealand= new google.maps.LatLng(-42.488302,172.039032);
	 	return New_Zealand;
	}
	if(name=='Norway')
	{
	 	var Norway= new google.maps.LatLng(64.168107,11.939392);
	 	return Norway;
	}
	if(name=='Philippines')
	{
	 	var Philippines= new google.maps.LatLng(13.539201,122.117157);
	 	return Philippines;
	}
	if(name=='Poland')
	{
	 	var Poland= new google.maps.LatLng(52.079506,19.244614);
	 	return Poland;
	}
	if(name=='Portugal')
	{
	 	var Portugal= new google.maps.LatLng(39.842286,-8.023453);
	 	return Portugal;
	}
	if(name=='Romania')
	{
	 	var Romania= new google.maps.LatLng(46.073231,24.715805);
	 	return Romania;
	}
	if(name=='Russia')
	{
	 	var Russia= new google.maps.LatLng(55.178868,33.898315);
	 	return Russia;
	}
	if(name=='Serbia')
	{
	 	var Serbia= new google.maps.LatLng(44.260937,20.694809);
	 	return Serbia;
	}
	if(name=='Singapore')
	{
	 	var Singapore= new google.maps.LatLng(1.363549,103.801467);
	 	return Singapore;
	}
	if(name=='Slovakia')
	{
	 	var Slovakia= new google.maps.LatLng(48.669199,18.86816);
	 	return Slovakia;
	}
	if(name=='Slovenia')
	{
	 	var Slovenia= new google.maps.LatLng(46.115134,14.715328);
	 	return Slovenia;
	}
	if(name=='South Africa')
	{
	 	var South_Africa= new google.maps.LatLng(-29.458731,24.77829);
	 	return South_Africa;
	}
	if(name=='Spain')
	{
	 	var Spain= new google.maps.LatLng(40.813809,-3.917999);
	 	return Spain;
	}
	if(name=='Sweden')
	{
	 	var Sweden= new google.maps.LatLng(61.648162,14.576111);
	 	return Sweden;
	}
	if(name=='Switzerland')
	{
	 	var Switzerland= new google.maps.LatLng(46.830134,7.859344);
	 	return Switzerland;
	}
	if(name=='Turkey')
	{
	 	var Turkey= new google.maps.LatLng(39.909736,35.764618);
	 	return Turkey;
	}
	if(name=='UK')
	{
	 	var UK= new google.maps.LatLng(53.852527,-2.59964);
	 	return UK;
	}
	if(name=='US')
	{
	 	var US= new google.maps.LatLng(38.959409,-99.871216);
	 	return US;
	}
	
	
}
