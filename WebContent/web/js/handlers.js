/*************************************************************
 * handlers.js
 * 
 * Handling the file upload procedure with Flash
 *************************************************************/

function preLoad() {
	if (!this.support.loading) {
		alert("You need the Flash Player to use SWFUpload.");
		return false;
	} else if (!this.support.imageResize) {
		alert("You need Flash Player 10 to upload resized images.");
		return false;
	}
}
function loadFailed() {
	alert("Something went wrong while loading SWFUpload. If this were a real application we'd clean up and then give you an alternative");
}

function fileQueued(file) {
	try {
		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		progress.setStatus("");
		progress.toggleCancel(true, this);

	} catch (ex) {
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("You have attempted to queue too many files.\n"
					+ (message === 0 ? "You have reached the upload limit."
							: "You may select "
									+ (message > 1 ? "up to " + message
											+ " files." : "one file.")));
			return;
		}

		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			progress.setStatus("File is too big.");
			this.debug("Error Code: File too big, File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			progress.setStatus("Cannot upload Zero Byte files.");
			this.debug("Error Code: Zero byte file, File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			progress.setStatus("Invalid File Type.");
			this.debug("Error Code: Invalid File Type, File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		default:
			if (file !== null) {
				progress.setStatus("Unhandled Error");
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (numFilesQueued > 0) {
			// this.startResizedUpload(this.getFile(0).ID,
			// this.customSettings.thumbnail_width,
			// this.customSettings.thumbnail_height,
			// SWFUpload.RESIZE_ENCODING.JPEG,
			// this.customSettings.thumbnail_quality, false);
		}

		/* I want auto start the upload and I can do that here */
		this.startUpload();
	} catch (ex) {
		this.debug(ex);
	}
}
function uploadStart(file) {
	try {
		/*
		 * I don't want to do any file validation or anything, I'll just update
		 * the UI and return true to indicate that the upload should start. It's
		 * important to update the UI here because in Linux no uploadProgress
		 * events are called. The best we can do is say we are uploading.
		 */
		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		progress.setStatus("uploading...");
		progress.toggleCancel(true, this);
	} catch (ex) {
	}

	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	try {
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		progress.setProgress(percent);
		progress.setStatus("uploading...");
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData) {
	try {
		
		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		var json = $.parseJSON(serverData);
		if (json.success) {
			progress.setComplete();
			progress.setStatus("");
			progress.toggleCancel(false);
			addImage("../" + json.imagePath, json.topOne, json.appealOne,json.topTwo, json.appealTwo,json.topThree, json.appealThree);
			$("#idImg").attr("src", "../" + json.imagePath);
			var length = document.getElementById("thumbnails").length;
			$("#test").attr("value",length);
			
			addMarker(json.topOne,json.topTwo,json.topThree,json.appealOne,json.appealTwo,json.appealThree);
			$("#download").attr("href","../"+json.csvPath);		
			// $("#targetCT").css({height:json.height+'px',width:json.width+"px"});
		} else {
			progress.setStatus("fail.");
			progress.toggleCancel(false);
			alert(serverData);
		}
	} catch (ex) {
		alert(ex);
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	try {
		var progress = new FileProgress(file,
				this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			progress.setStatus("Upload Error: " + message);
			this.debug("Error Code: HTTP Error, File name: " + file.name
					+ ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			progress.setStatus("Upload Failed.");
			this.debug("Error Code: Upload Failed, File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			progress.setStatus("Server (IO) Error");
			this.debug("Error Code: IO Error, File name: " + file.name
					+ ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			progress.setStatus("Security Error");
			this.debug("Error Code: Security Error, File name: " + file.name
					+ ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			progress.setStatus("Upload limit exceeded.");
			this.debug("Error Code: Upload Limit Exceeded, File name: "
					+ file.name + ", File size: " + file.size + ", Message: "
					+ message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			progress.setStatus("Failed Validation.  Upload skipped.");
			this.debug("Error Code: File Validation Failed, File name: "
					+ file.name + ", File size: " + file.size + ", Message: "
					+ message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			// If there aren't any files left (they were all cancelled) disable
			// the cancel button
			if (this.getStats().files_queued === 0) {
				// document.getElementById(this.customSettings.cancelButtonId).disabled
				// = true;
			}
			progress.setStatus("Cancelled");
			progress.setCancelled();
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			progress.setStatus("Stopped");
			break;
		default:
			progress.setStatus("Unhandled Error: " + errorCode);
			this.debug("Error Code: " + errorCode + ", File name: " + file.name
					+ ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadComplete(file) {
	try {
		if (this.getStats().files_queued === 0) {
			// document.getElementById(this.customSettings.cancelButtonId).disabled
			// = true;
		}
	} catch (ex) {
		this.debug(ex);
	}
}

function addImage(src, topone,appealone,toptwo,appealtwo,topthree,appealthree) {
	var newImg = document.createElement("img");
	newImg.style.maxWidth = "200px";
	newImg.style.maxHeight = "120px";
	newImg.setAttribute("topone",topone);
	newImg.setAttribute("appealone",appealone);
	newImg.setAttribute("toptwo",toptwo);
	newImg.setAttribute("appealtwo",appealtwo);
	newImg.setAttribute("topthree",topthree);
	newImg.setAttribute("appealthree",appealthree);
	//when this function invoke, the thumbnails section is not empty
	document.getElementById("thumbnails").setAttribute("name","1");
	
	document.getElementById("thumbnails").appendChild(newImg);
	if (newImg.filters) {
		try {
			newImg.filters.item("DXImageTransform.Microsoft.Alpha").opacity = 0;
		} catch (e) {
			// If it is not set initially, the browser will throw an error. This
			// will set it if it is not set yet.
			newImg.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity='
					+ 0 + ')';
		}
	} else {
		newImg.style.opacity = 0;
	}

	newImg.onload = function() {
		fadeIn(newImg, 0);
	};
	newImg.src = src;
}

function setUp(){
		$.ajax({
			url:'update',
			type: 'POST'			
		});
}





function fadeIn(element, opacity) {
	var reduceOpacityBy = 5;
	var rate = 30; // 15 fps

	if (opacity < 100) {
		opacity += reduceOpacityBy;
		if (opacity > 100) {
			opacity = 100;
		}

		if (element.filters) {
			try {
				element.filters.item("DXImageTransform.Microsoft.Alpha").opacity = opacity;
			} catch (e) {
				// If it is not set initially, the browser will throw an error.
				// This will set it if it is not set yet.
				element.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity='
						+ opacity + ')';
			}
		} else {
			element.style.opacity = opacity / 100;
		}
	}

	if (opacity < 100) {
		setTimeout(function() {
			fadeIn(element, opacity);
		}, rate);
	}
}
