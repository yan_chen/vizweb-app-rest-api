package com.rest.Computation;

import java.util.Arrays;
import java.util.Comparator;

public class Appeal {

	public static String[] Appeal(double color, double complexity) throws NullPointerException {

		Country AllCountryData[] = new Country[48];
		AllCountryData = Model.ModelData();
		
		String result[][] = new String[48][2];
		int i;
		//calculate all the appeal values by country
		for (i = 0; i < AllCountryData.length; i++) {
			result[i][1] = AllCountryData[i].getName();
			result[i][0] = Double.toString(Math.pow(color, 2)
					* AllCountryData[i].getColorfulnessModel()
					+ Math.pow(complexity, 2) * AllCountryData[i].getComplexityModel());
			
			AllCountryData[i].setCompute(Double.parseDouble(result[i][0]));
		}
		//sort the appeal results
		Arrays.sort(result, new Comparator<String[]>() {
			
            public int compare(final String[] entry1, final String[] entry2) {
                    final double time11 = Double.parseDouble(entry1[0])*1000;
                    final double time22 = Double.parseDouble(entry2[0])*1000;               
                	return (int)(time11-time22);                    
            }
    });
		
		String TopThree[] = new String[6];
		
		TopThree[0] = result[47][1];
		TopThree[1] = result[46][1];
		TopThree[2] = result[45][1];
		TopThree[3] = result[47][0];
		TopThree[4] = result[46][0];
		TopThree[5] = result[45][0];
		return TopThree;
	}

}
