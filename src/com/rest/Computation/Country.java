package com.rest.Computation;

public class Country {
	public String Name;
	public double ColorfulnessModel;
	public double ComplexityModel;
	public double Compute;
	
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public double getColorfulnessModel() {
		return ColorfulnessModel;
	}
	public void setColorfulnessModel(double colorfulnessModel) {
		ColorfulnessModel = colorfulnessModel;
	}
	public double getComplexityModel() {
		return ComplexityModel;
	}
	public void setComplexityModel(double complexityModel) {
		ComplexityModel = complexityModel;
	}
	public double getCompute() {
		return Compute;
	}
	public void setCompute(double compute) {
		Compute = compute;
	}
	
	
}
