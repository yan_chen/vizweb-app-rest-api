package com.rest.Computation;

import com.googlecode.javacv.CanvasFrame.Exception;

public class Model {

	//The colorfulness and complexity values associated with all countries
	public static Country[] ModelData() throws NullPointerException{

		Country ModelData[] = new Country[48];
		
		for(int i=0;i<48;i++)
		{
			ModelData[i] = new Country();
		}
	
		ModelData[0].setName("Australia");		
		ModelData[0].setColorfulnessModel(-0.0369409);
		ModelData[0].setComplexityModel(-0.980449);
	
		ModelData[1].setName("Austria");
		ModelData[1].setColorfulnessModel(-0.1013167);
		ModelData[1].setComplexityModel(-0.1453129);

		ModelData[2].setName("Belgium");
		ModelData[2].setColorfulnessModel(-0.0380783);
		ModelData[2].setComplexityModel(-0.1310817);

		ModelData[3].setName("Bosnia And Herzegovina");
		ModelData[3].setColorfulnessModel(0.2975410);
		ModelData[3].setComplexityModel(0.2222502);

		ModelData[4].setName("Brazil");
		ModelData[4].setColorfulnessModel(0.0067241);
		ModelData[4].setComplexityModel(-0.1445599);

		ModelData[5].setName("Bulgaria");
		ModelData[5].setColorfulnessModel(-0.0466407);
		ModelData[5].setComplexityModel(0.0207571);

		ModelData[6].setName("Canada");
		ModelData[6].setColorfulnessModel(0.0354462);
		ModelData[6].setComplexityModel(-0.0780695);

		ModelData[7].setName("Chile");
		ModelData[7].setColorfulnessModel(0.2656081);
		ModelData[7].setComplexityModel(-0.0128304);

		ModelData[8].setName("China");
		ModelData[8].setColorfulnessModel(-0.0157981);
		ModelData[8].setComplexityModel(0.0274691);

		ModelData[9].setName("Colombia");
		ModelData[9].setColorfulnessModel(0.1198257);
		ModelData[9].setComplexityModel(-0.1205784);

		ModelData[10].setName("Croatia");
		ModelData[10].setColorfulnessModel(0.2319118);
		ModelData[10].setComplexityModel(-0.1254168);

		ModelData[11].setName("Czech");
		ModelData[11].setColorfulnessModel(-0.2497060);
		ModelData[11].setComplexityModel(0.0854524);

		ModelData[12].setName("Denmark");
		ModelData[12].setColorfulnessModel(-0.1263640);
		ModelData[12].setComplexityModel(-0.1086306);

		ModelData[13].setName("Finland");
		ModelData[13].setColorfulnessModel(-0.1052682);
		ModelData[13].setComplexityModel(-0.0892437);

		ModelData[14].setName("France");
		ModelData[14].setColorfulnessModel(-0.1284874);
		ModelData[14].setComplexityModel(0.0104588);

		ModelData[15].setName("Germany");
		ModelData[15].setColorfulnessModel(-0.0801896);
		ModelData[15].setComplexityModel(-0.1518057);

		ModelData[16].setName("Greece");
		ModelData[16].setColorfulnessModel(0.0462500);
		ModelData[16].setComplexityModel(0.0243064);

		ModelData[17].setName("Hungary");
		ModelData[17].setColorfulnessModel(0.0448821);
		ModelData[17].setComplexityModel(0.0771640);

		ModelData[18].setName("India");
		ModelData[18].setColorfulnessModel(0.0358139);
		ModelData[18].setComplexityModel(0.0173379);

		ModelData[19].setName("Indonesia");
		ModelData[19].setColorfulnessModel(0.0792977);
		ModelData[19].setComplexityModel(-0.0204536);

		ModelData[20].setName("Iran");
		ModelData[20].setColorfulnessModel(0.0530945);
		ModelData[20].setComplexityModel(-0.0033574);

		ModelData[21].setName("Ireland");
		ModelData[21].setColorfulnessModel(0.1599074);
		ModelData[21].setComplexityModel(-0.0463026);

		ModelData[22].setName("Israel");
		ModelData[22].setColorfulnessModel(0.0113344);
		ModelData[22].setComplexityModel(-0.0835224);

		ModelData[23].setName("Italy");
		ModelData[23].setColorfulnessModel(0.0077573);
		ModelData[23].setComplexityModel(-0.0691993);

		ModelData[24].setName("Japan");
		ModelData[24].setColorfulnessModel(0.0795018);
		ModelData[24].setComplexityModel(0.0058809);

		ModelData[25].setName("Lithuania");
		ModelData[25].setColorfulnessModel(0.0194933);
		ModelData[25].setComplexityModel(0.0372231);

		ModelData[26].setName("Macedonia");
		ModelData[26].setColorfulnessModel(0.1852488);
		ModelData[26].setComplexityModel(0.1412066);

		ModelData[27].setName("Malaysia");
		ModelData[27].setColorfulnessModel(0.1573775);
		ModelData[27].setComplexityModel(-0.0154434);

		ModelData[28].setName("Mexico");
		ModelData[28].setColorfulnessModel(0.2552101);
		ModelData[28].setComplexityModel(-0.0612755);

		ModelData[29].setName("Netherlands");
		ModelData[29].setColorfulnessModel(-0.0006424);
		ModelData[29].setComplexityModel(-0.0609807);

		ModelData[30].setName("New Zealand");
		ModelData[30].setColorfulnessModel(0.0685485);
		ModelData[30].setComplexityModel(-0.0034510);

		ModelData[31].setName("Norway");
		ModelData[31].setColorfulnessModel(-0.0685403);
		ModelData[31].setComplexityModel(-0.0663482);

		ModelData[32].setName("Philippines");
		ModelData[32].setColorfulnessModel(-0.0593332);
		ModelData[32].setComplexityModel(-0.0558735);

		ModelData[33].setName("Poland");
		ModelData[33].setColorfulnessModel(-0.0880078);
		ModelData[33].setComplexityModel(-0.0506920);

		ModelData[34].setName("Portugal");
		ModelData[34].setColorfulnessModel(-0.0682362);
		ModelData[34].setComplexityModel(-0.0980775);

		ModelData[35].setName("Romania");
		ModelData[35].setColorfulnessModel(0.0848687);
		ModelData[35].setComplexityModel(0.0768402);

		ModelData[36].setName("Russia");
		ModelData[36].setColorfulnessModel(-0.2614767);
		ModelData[36].setComplexityModel(-0.0511550);

		ModelData[37].setName("Serbia");
		ModelData[37].setColorfulnessModel(0.1741302);
		ModelData[37].setComplexityModel(0.0671475);

		ModelData[38].setName("Singapore");
		ModelData[38].setColorfulnessModel(0.1479014);
		ModelData[38].setComplexityModel(0.0931451);

		ModelData[39].setName("Slovakia");
		ModelData[39].setColorfulnessModel(-0.0336455);
		ModelData[39].setComplexityModel(0.0364415);

		ModelData[40].setName("Slovenia");
		ModelData[40].setColorfulnessModel(0.3163747);
		ModelData[40].setComplexityModel(-0.0863890);

		ModelData[41].setName("South Africa");
		ModelData[41].setColorfulnessModel(0.0117560);
		ModelData[41].setComplexityModel(0.0470526);

		ModelData[42].setName("Spain");
		ModelData[42].setColorfulnessModel(-0.1287207);
		ModelData[42].setComplexityModel(-0.0008753);

		ModelData[43].setName("Sweden");
		ModelData[43].setColorfulnessModel(-0.0778352);
		ModelData[43].setComplexityModel(-0.1384001);

		ModelData[44].setName("Switzerland");
		ModelData[44].setColorfulnessModel(-0.0986729);
		ModelData[44].setComplexityModel(-0.1153806);

		ModelData[45].setName("Turkey");
		ModelData[45].setColorfulnessModel(-0.0702349);
		ModelData[45].setComplexityModel(-0.0109205);

		ModelData[46].setName("UK");
		ModelData[46].setColorfulnessModel(0.1295261);
		ModelData[46].setComplexityModel(-0.0220338);

		ModelData[47].setName("US");
		ModelData[47].setColorfulnessModel(0.0264625);
		ModelData[47].setComplexityModel(-0.0741317);

		return ModelData;
	}
}
