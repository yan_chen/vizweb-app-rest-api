package com.rest.Computation;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import javax.imageio.ImageIO;

import org.sikuli.design.QuadtreeFeatureComputer;
import org.sikuli.design.XYFeatureComputer;
import org.sikuli.design.color.ColorAnalyzer;
import org.sikuli.design.color.NamedColor;
import org.sikuli.design.color.StandardColors;
import org.sikuli.design.quadtree.ColorEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.IntensityEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.QuadTreeDecomposer;
import org.sikuli.design.quadtree.QuadTreeDecompositionStrategy;
import org.sikuli.design.quadtree.Quadtree;
import org.sikuli.design.structure.Block;
import org.sikuli.design.xycut.DefaultXYDecompositionStrategy;
import org.sikuli.design.xycut.XYDecomposer;
import org.sikuli.design.xycut.XYDecompositionStrategy;
import org.sikuli.design.xycut.XYTextDetector;

import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.rest.bean.ImageMeta;


public class imageProcess{
	static String csv;
	public static ImageMeta imageProcess(BufferedImage input, String path, String hasimage)
			throws IOException {
		ImageMeta meta = new ImageMeta();
		
		
		String[] re = {"0","0","0","0","0","0"};
		if(hasimage.equals("0"))
			re=generateCsvFile(input, path);
		else
			re=appendCsvFile(input, path);
		
		try {
			meta.setTopOne(re[0]);			
			meta.setTopTwo(re[1]);
			meta.setTopThree(re[2]);
			meta.setAppealOne(Double.parseDouble(re[3]));
			meta.setAppealTwo(Double.parseDouble(re[4]));
			meta.setAppealThree(Double.parseDouble(re[5]));
			
			meta.setSuccess(true);
			meta.setMsg("Digital image succeed");
		} catch (Exception e) {
			meta.setSuccess(false);
			meta.setMsg("Digital image processing error " + e.getMessage());
		}
		
		return meta;
	
	}

	
	public static String[] generateCsvFile(BufferedImage input, String inputFileName) throws IOException {
		
		csv = inputFileName.substring(0,
				inputFileName.length() - 3) + "csv";
		System.out.println("generate csv file path "+inputFileName);
		double col[] = new double[13];
		double cop[] = new double[7];
		String outputFileName = inputFileName.substring(0,
				inputFileName.length() - 3)
				+ "xml";
		String re[] = new String[6];
		
		StandardColors cn = new StandardColors();		
		File file = new File(inputFileName);
	
		try {
			FileWriter writer = new FileWriter(csv);
	
			StandardColors colorNames = new StandardColors();
			writer.append("filename,");
			for (NamedColor c : colorNames) {
				writer.append(c.getName() + ",");
			}
	
			writer.append("hue,saturation,value,textArea,nonTextArea,maxLevel,"
					+ "averageLevel,numOfLeaves,numOf1stLevelNodes,"
					+ "numOf2ndLevelNodes,numOf3rdLevelNodes,percentageOfLeafArea,"
					+ "percentageOfLeafArea2,numOfTextGroup,numOfImageArea,"
					+ "colorfulness1,colorfulness2,"
					+ "colorNumQuadTreeLeaves," + "intensityNumQuadTreeLeaves,"
					+ "colorHorizontalSymmetry," + "colorVerticalSymmetry,"
					+ "colorHorizontalBalance," + "colorVerticalBalance,"
					+ "intensityHorizontalSymmetry,"
					+ "intensityVerticalSymmetry,"
					+ "intensityHorizontalBalance,"
					+ "intensityVerticalBalance," + "colorEquilibrium,"
					+ "intensityEquilibrium," +"Colorfulness,"+"Complexity\n");
	
			// for (int ii = 0; ii < files.length; ii++) {
	
			// File file = files[ii];
			System.out.println("Processing: " + file.getName());
	
			try {
	
				writer.append(file.getName() + ",");
	
				Map<NamedColor, Double> d = ColorAnalyzer.computeColorDistribution(input);
	
				col[0] = d.get(cn.colors.get(2));
				col[1] = d.get(cn.colors.get(3));
				col[2] = d.get(cn.colors.get(4));
				col[3] = d.get(cn.colors.get(8));
				col[4] = d.get(cn.colors.get(9));
				col[5] = d.get(cn.colors.get(13));
				col[6] = d.get(cn.colors.get(14));
				for (NamedColor c : colorNames.colors) {
					writer.append(d.get(c) + ",");
	
				}
	
				CvScalar avg = ColorAnalyzer
						.computeAverageHueSaturationValue(input);
				writer.append(avg.getVal(0) + "," + avg.getVal(1) + ","
						+ avg.getVal(2) + ",");
				col[7] = avg.getVal(1);
				cop[6] = avg.getVal(0);
			} catch (IOException ioe) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + ioe.getMessage());
				writer.append("io_failed_"
						+ file.getName()
						+ ",N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,");
			} catch (Exception e) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + e.getMessage());
				writer.append("failed_"
						+ file.getName()
						+ ",N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,");
			}
	
			File xmlfile = new File(outputFileName);
			
			System.out.println(xmlfile.getName());
			Map<NamedColor, Double> dd = ColorAnalyzer.computeColorDistribution(input);
			
			for (NamedColor c : cn.colors) {
				System.out.println(dd.get(c) + ",");
			}
			decomposeSingleImage(input, inputFileName, outputFileName);
			if (xmlfile
					.getName()
					.substring(0, xmlfile.getName().length() - 4)
					.equals(file.getName().substring(0,
							xmlfile.getName().length() - 4))) {
	
				Block rootWithTextdetected = Block.loadFromXml(xmlfile
						.getAbsolutePath());
	
				int textArea = 0;
				textArea =	XYFeatureComputer.computeTextArea(rootWithTextdetected);
	
				int nonTextLeavesArea =0;
				nonTextLeavesArea = XYFeatureComputer
						.computeNonTextLeavesArea(rootWithTextdetected);
	
				int maxLevel = 0;
				maxLevel=	XYFeatureComputer
						.computeMaximumDecompositionLevel(rootWithTextdetected);
	
				double avgLevel = 0;
				avgLevel=XYFeatureComputer
						.computeAverageDecompositionLevel(rootWithTextdetected);
	
				int numOfLeaves = 0;
				numOfLeaves=XYFeatureComputer
						.countNumberOfLeaves(rootWithTextdetected);
	
				int numOf1stLevelNodes =0;
				numOf1stLevelNodes	= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 1);
	
				int numOf2ndLevelNodes=0;
				numOf2ndLevelNodes= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 2);
	
				int numOf3rdLevelNodes=0;
				numOf3rdLevelNodes= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 3);
	
				double percentageLeafArea=0;
				percentageLeafArea= XYFeatureComputer
						.computePercentageOfLeafArea(rootWithTextdetected);
	
				double percentageLeafArea2=0;
				percentageLeafArea2= XYFeatureComputer
						.computePercentageOfLeafArea2(rootWithTextdetected);
	
				int numOfTextGroup=0;
				numOfTextGroup= XYFeatureComputer
						.countNumberOfTextGroup(rootWithTextdetected);
	
				int numOfImageArea=0;
				numOfImageArea= XYFeatureComputer
						.countNumberOfImageArea(rootWithTextdetected);
				col[9] = numOfImageArea;
				col[11] = textArea;
				col[12] = nonTextLeavesArea;
				cop[0] = textArea;
				cop[1] = nonTextLeavesArea;
				cop[2] = numOfLeaves;
				cop[3] = numOfTextGroup;
				cop[4] = numOfImageArea;
	
				writer.append(
	
				textArea + "," + nonTextLeavesArea + "," + maxLevel + ","
						+ avgLevel + "," + numOfLeaves + ","
						+ numOf1stLevelNodes + "," + numOf2ndLevelNodes + ","
						+ numOf3rdLevelNodes + "," + percentageLeafArea + ","
						+ percentageLeafArea2 + "," + numOfTextGroup + ","
						+ numOfImageArea + ","
				// numOfTextLeaves + "," +
				// (numOfLeaves-numOfTextLeaves)
				);
	
			}
	
			try {
	
				// colorfulness 1
				double colorfulness1 = 0;
				colorfulness1 = ColorAnalyzer.computeColorfulness(input);
				writer.append( colorfulness1 + ",");
				col[8] = ColorAnalyzer.computeColorfulness(input);
	
				// colorfulness 2
				double colorfulness2 = 0;
				colorfulness2 = ColorAnalyzer.computeColorfulness2(input);
				writer.append( colorfulness2 + ",");
				cop[5] = colorfulness2;
	
				QuadTreeDecompositionStrategy iStrategy = new IntensityEntropyDecompositionStrategy();
				QuadTreeDecompositionStrategy cStrategy = new ColorEntropyDecompositionStrategy();
	
				QuadTreeDecomposer cDecomposer = null; 
				cDecomposer = new QuadTreeDecomposer(cStrategy);
				Quadtree cRoot = null; 
				cRoot = cDecomposer.decompose(input);
	
				QuadTreeDecomposer iDecomposer = null;
				iDecomposer = new QuadTreeDecomposer(
						iStrategy);
				Quadtree iRoot = null;
				iRoot = iDecomposer.decompose(input);
	
				Point centerOfImage = new Point(input.getWidth() / 2,
						input.getHeight() / 2);
				double colorEquilibrium=0;
				colorEquilibrium= QuadtreeFeatureComputer
						.computeEquilibrium(cRoot, centerOfImage);
				double intensityEquilibrium=0;
				intensityEquilibrium= QuadtreeFeatureComputer
						.computeEquilibrium(iRoot, centerOfImage);
	
				double colorHorizontalSymmetry=0;
				colorHorizontalSymmetry= QuadtreeFeatureComputer
						.computeHorizontalSymmetry(cRoot);
				double colorVerticalSymmetry=0;
				colorVerticalSymmetry= QuadtreeFeatureComputer
						.computeVerticalSymmetry(cRoot);
				double colorHorizontalBalance=0;
				colorHorizontalBalance= QuadtreeFeatureComputer
						.computeHorizontalBalance(cRoot);
				double colorVerticalBalance=0;
				colorVerticalBalance= QuadtreeFeatureComputer
						.computeVerticalBalance(cRoot);
	
				double intensityHorizontalSymmetry=0;
				intensityHorizontalSymmetry= QuadtreeFeatureComputer
						.computeHorizontalSymmetry(iRoot);
				double intensityVerticalSymmetry=0;
				intensityVerticalSymmetry= QuadtreeFeatureComputer
						.computeVerticalSymmetry(iRoot);
				double intensityHorizontalBalance=0;
				intensityHorizontalBalance= QuadtreeFeatureComputer
						.computeHorizontalBalance(iRoot);
				double intensityVerticalBalance=0;
				intensityVerticalBalance= QuadtreeFeatureComputer
						.computeVerticalBalance(iRoot);
	
				int colorNumQuadTreeLeaves=0;
				colorNumQuadTreeLeaves= cRoot.countLeaves() / 4;
				int intensityNumQuadTreeLeaves=0;
				intensityNumQuadTreeLeaves= iRoot.countLeaves() / 4;
				col[10] = colorNumQuadTreeLeaves;
	
				writer.append(colorNumQuadTreeLeaves + ","
						+ intensityNumQuadTreeLeaves + ","
						+ colorHorizontalSymmetry + "," + colorVerticalSymmetry
						+ "," + colorHorizontalBalance + ","
						+ colorVerticalBalance + ","
						+ intensityHorizontalSymmetry + ","
						+ intensityVerticalSymmetry + ","
						+ intensityHorizontalBalance + ","
						+ intensityVerticalBalance + "," + colorEquilibrium
						+ "," + intensityEquilibrium+",");
	
			} catch (IOException ioe) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + ioe.getMessage());
				writer.append("io_failed_" + file.getName() + ",,,,,,,,,,,,");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + e.getMessage());
				writer.append("failed_" + file.getName() + ",,,,,,,,,,,,");
			}
	
			
			// return value section
			double colorfulness = -0.68 + 2.45 * col[0] + 2.74 * col[1] + 1.60
					* col[2] + 1.85 * col[3] - 3.17 * col[4] - 3.91 * col[5]
							+1.01*col[6] +0.01*col[7]+0.03*col[8]+0.06*col[9]
									+3.74*Math.pow(10,-4)*col[10] -1.48*Math.pow(10, -6)*col[11]
											+1.86*Math.pow(10, -6)*col[12];
			double complexity = 0.637 +0.005*cop[2] +0.052*cop[3] +0.056*cop[4]
					+0.011*cop[5] +0.005*cop[6];
			
			writer.append(colorfulness + ","+complexity);
			System.out.println("here we go");
			System.out.println(colorfulness+"	"+complexity);
			writer.append("\n");
			
			re = Appeal.Appeal(colorfulness, complexity);
			writer.flush();
			writer.close();
			
		} catch (IOException ioe) {
	
		}
		return re;
	}
	
	public static void decomposeSingleImage(BufferedImage inputImage,
			String inputFileName, String outputFileName) throws IOException {
	
		// IplImage gray =
		// VisionUtils.createGrayImageFrom(IplImage.createFrom(inputImage));
	
		XYDecompositionStrategy strategy = new DefaultXYDecompositionStrategy() {
	
			@Override
			public int getMaxLevel() {
				return 10;
			}
	
			@Override
			public boolean isSplittingHorizontally() {
				return true;
			}
	
			@Override
			public boolean isSplittingVertically() {
				return true;
			}
	
			@Override
			public int getMinSeperatorSize() {
				return 4;
			}
		};
	
		XYDecomposer d = new XYDecomposer();
		Block root = d.decompose(inputImage,
				strategy);
	
		
		root.filterOutSmallBlocks();
	
		
		XYTextDetector td = new XYTextDetector(root, inputImage);
		Block rootWithTextdetected = td.detect();
	
		// rootWithTextdetected.filterOutSmallBlocks();
	
		rootWithTextdetected.toXML(inputFileName, outputFileName);
		
		
	}
	
public static String[] appendCsvFile(BufferedImage input, String inputFileName) throws IOException {
		
		
		System.out.println("path "+inputFileName);
		double col[] = new double[13];
		double cop[] = new double[7];
		String outputFileName = inputFileName.substring(0,
				inputFileName.length() - 3)
				+ "xml";
		String re[] = new String[6];
		
		StandardColors cn = new StandardColors();		
		File file = new File(inputFileName);
	
		try {
			FileWriter writer = new FileWriter(csv,true);
	
			StandardColors colorNames = new StandardColors();
			
			System.out.println("Processing: " + file.getName());
	
			try {
	
				writer.append(file.getName() + ",");
	
				Map<NamedColor, Double> d = ColorAnalyzer.computeColorDistribution(input);
	
				col[0] = d.get(cn.colors.get(2));
				col[1] = d.get(cn.colors.get(3));
				col[2] = d.get(cn.colors.get(4));
				col[3] = d.get(cn.colors.get(8));
				col[4] = d.get(cn.colors.get(9));
				col[5] = d.get(cn.colors.get(13));
				col[6] = d.get(cn.colors.get(14));
				for (NamedColor c : colorNames.colors) {
					writer.append(d.get(c) + ",");
	
				}
	
				CvScalar avg = ColorAnalyzer
						.computeAverageHueSaturationValue(input);
				writer.append(avg.getVal(0) + "," + avg.getVal(1) + ","
						+ avg.getVal(2) + ",");
				col[7] = avg.getVal(1);
				cop[6] = avg.getVal(0);
			} catch (IOException ioe) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + ioe.getMessage());
				writer.append("io_failed_"
						+ file.getName()
						+ ",N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,");
			} catch (Exception e) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + e.getMessage());
				writer.append("failed_"
						+ file.getName()
						+ ",N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,N/A,");
			}
	
			File xmlfile = new File(outputFileName);
			
			System.out.println(xmlfile.getName());
			Map<NamedColor, Double> dd = ColorAnalyzer.computeColorDistribution(input);
			
			for (NamedColor c : cn.colors) {
				System.out.println(dd.get(c) + ",");
			}
			decomposeSingleImage(input, inputFileName, outputFileName);
			if (xmlfile
					.getName()
					.substring(0, xmlfile.getName().length() - 4)
					.equals(file.getName().substring(0,
							xmlfile.getName().length() - 4))) {
	
				Block rootWithTextdetected = Block.loadFromXml(xmlfile
						.getAbsolutePath());
	
				int textArea = 0;
				textArea =	XYFeatureComputer.computeTextArea(rootWithTextdetected);
	
				int nonTextLeavesArea =0;
				nonTextLeavesArea = XYFeatureComputer
						.computeNonTextLeavesArea(rootWithTextdetected);
	
				int maxLevel = 0;
				maxLevel=	XYFeatureComputer
						.computeMaximumDecompositionLevel(rootWithTextdetected);
	
				double avgLevel = 0;
				avgLevel=XYFeatureComputer
						.computeAverageDecompositionLevel(rootWithTextdetected);
	
				int numOfLeaves = 0;
				numOfLeaves=XYFeatureComputer
						.countNumberOfLeaves(rootWithTextdetected);
	
				int numOf1stLevelNodes =0;
				numOf1stLevelNodes	= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 1);
	
				int numOf2ndLevelNodes=0;
				numOf2ndLevelNodes= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 2);
	
				int numOf3rdLevelNodes=0;
				numOf3rdLevelNodes= XYFeatureComputer
						.countNumberOfNodesInLevel(rootWithTextdetected, 3);
	
				double percentageLeafArea=0;
				percentageLeafArea= XYFeatureComputer
						.computePercentageOfLeafArea(rootWithTextdetected);
	
				double percentageLeafArea2=0;
				percentageLeafArea2= XYFeatureComputer
						.computePercentageOfLeafArea2(rootWithTextdetected);
	
				int numOfTextGroup=0;
				numOfTextGroup= XYFeatureComputer
						.countNumberOfTextGroup(rootWithTextdetected);
	
				int numOfImageArea=0;
				numOfImageArea= XYFeatureComputer
						.countNumberOfImageArea(rootWithTextdetected);
				col[9] = numOfImageArea;
				col[11] = textArea;
				col[12] = nonTextLeavesArea;
				cop[0] = textArea;
				cop[1] = nonTextLeavesArea;
				cop[2] = numOfLeaves;
				cop[3] = numOfTextGroup;
				cop[4] = numOfImageArea;
	
				writer.append(
	
				textArea + "," + nonTextLeavesArea + "," + maxLevel + ","
						+ avgLevel + "," + numOfLeaves + ","
						+ numOf1stLevelNodes + "," + numOf2ndLevelNodes + ","
						+ numOf3rdLevelNodes + "," + percentageLeafArea + ","
						+ percentageLeafArea2 + "," + numOfTextGroup + ","
						+ numOfImageArea + ","
				// numOfTextLeaves + "," +
				// (numOfLeaves-numOfTextLeaves)
				);
	
			}
	
			try {
	
				// colorfulness 1
				double colorfulness1 = 0;
				colorfulness1 = ColorAnalyzer.computeColorfulness(input);
				writer.append( colorfulness1 + ",");
				col[8] = ColorAnalyzer.computeColorfulness(input);
	
				// colorfulness 2
				double colorfulness2 = 0;
				colorfulness2 = ColorAnalyzer.computeColorfulness2(input);
				writer.append( colorfulness2 + ",");
				cop[5] = colorfulness2;
	
				QuadTreeDecompositionStrategy iStrategy = new IntensityEntropyDecompositionStrategy();
				QuadTreeDecompositionStrategy cStrategy = new ColorEntropyDecompositionStrategy();
	
				QuadTreeDecomposer cDecomposer = null; 
				cDecomposer = new QuadTreeDecomposer(cStrategy);
				Quadtree cRoot = null; 
				cRoot = cDecomposer.decompose(input);
	
				QuadTreeDecomposer iDecomposer = null;
				iDecomposer = new QuadTreeDecomposer(
						iStrategy);
				Quadtree iRoot = null;
				iRoot = iDecomposer.decompose(input);
	
				Point centerOfImage = new Point(input.getWidth() / 2,
						input.getHeight() / 2);
				double colorEquilibrium=0;
				colorEquilibrium= QuadtreeFeatureComputer
						.computeEquilibrium(cRoot, centerOfImage);
				double intensityEquilibrium=0;
				intensityEquilibrium= QuadtreeFeatureComputer
						.computeEquilibrium(iRoot, centerOfImage);
	
				double colorHorizontalSymmetry=0;
				colorHorizontalSymmetry= QuadtreeFeatureComputer
						.computeHorizontalSymmetry(cRoot);
				double colorVerticalSymmetry=0;
				colorVerticalSymmetry= QuadtreeFeatureComputer
						.computeVerticalSymmetry(cRoot);
				double colorHorizontalBalance=0;
				colorHorizontalBalance= QuadtreeFeatureComputer
						.computeHorizontalBalance(cRoot);
				double colorVerticalBalance=0;
				colorVerticalBalance= QuadtreeFeatureComputer
						.computeVerticalBalance(cRoot);
	
				double intensityHorizontalSymmetry=0;
				intensityHorizontalSymmetry= QuadtreeFeatureComputer
						.computeHorizontalSymmetry(iRoot);
				double intensityVerticalSymmetry=0;
				intensityVerticalSymmetry= QuadtreeFeatureComputer
						.computeVerticalSymmetry(iRoot);
				double intensityHorizontalBalance=0;
				intensityHorizontalBalance= QuadtreeFeatureComputer
						.computeHorizontalBalance(iRoot);
				double intensityVerticalBalance=0;
				intensityVerticalBalance= QuadtreeFeatureComputer
						.computeVerticalBalance(iRoot);
	
				int colorNumQuadTreeLeaves=0;
				colorNumQuadTreeLeaves= cRoot.countLeaves() / 4;
				int intensityNumQuadTreeLeaves=0;
				intensityNumQuadTreeLeaves= iRoot.countLeaves() / 4;
				col[10] = colorNumQuadTreeLeaves;
	
				writer.append(colorNumQuadTreeLeaves + ","
						+ intensityNumQuadTreeLeaves + ","
						+ colorHorizontalSymmetry + "," + colorVerticalSymmetry
						+ "," + colorHorizontalBalance + ","
						+ colorVerticalBalance + ","
						+ intensityHorizontalSymmetry + ","
						+ intensityVerticalSymmetry + ","
						+ intensityHorizontalBalance + ","
						+ intensityVerticalBalance + "," + colorEquilibrium
						+ "," + intensityEquilibrium+",");
	
			} catch (IOException ioe) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + ioe.getMessage());
				writer.append("io_failed_" + file.getName() + ",,,,,,,,,,,,");
			} catch (Exception e) {
				System.out.println("Failed processing: " + file.getName()
						+ ". Error: " + e.getMessage());
				writer.append("failed_" + file.getName() + ",,,,,,,,,,,,");
			}
	
			
			// return value section
			double colorfulness = -0.68 + 2.45 * col[0] + 2.74 * col[1] + 1.60
					* col[2] + 1.85 * col[3] - 3.17 * col[4] - 3.91 * col[5]
							+1.01*col[6] +0.01*col[7]+0.03*col[8]+0.06*col[9]
									+3.74*Math.pow(10,-4)*col[10] -1.48*Math.pow(10, -6)*col[11]
											+1.86*Math.pow(10, -6)*col[12];
			double complexity = 0.637 +0.005*cop[2] +0.052*cop[3] +0.056*cop[4]
					+0.011*cop[5] +0.005*cop[6];
			
			writer.append(colorfulness + ","+complexity);
			
			writer.append("\n");
			
			re = Appeal.Appeal(colorfulness, complexity);
			writer.flush();
			writer.close();
			
		} catch (IOException ioe) {
	
		}
		return re;
	}
}
