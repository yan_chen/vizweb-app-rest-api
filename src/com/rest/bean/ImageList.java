package com.rest.bean;

import java.util.LinkedList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="images")
public class ImageList {

	private int count;
	private boolean success;
	private String msg;
	private LinkedList<ImageMeta> images;
	
	public ImageList() {}
	
	public ImageList(LinkedList<ImageMeta> images) {
		this.images = images;
		this.count = images.size();
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@XmlElement(name="image")
	public LinkedList<ImageMeta> getImages() {
		return images;
	}

	public void setImages(LinkedList<ImageMeta> images) {
		this.images = images;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
}
