package com.rest.bean;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "image")
public class ImageMeta {

	private double computeColorfulnessx;
	private double computeColorfulnessy;

	private int countNumberOfLeaves;
	private int countNumberOfTextGroup;

	private double computeHorizontalBalance;
	private double computeHorizontalSymmetry;

	private String imagePath;
	private String csvPath;
	private boolean success;
	private String msg;
	
	private String topOne;
	private String topTwo;
	private String topThree;

	private double appealOne;
	private double appealTwo;
	private double appealThree;
	
	public double getAppealOne() {
		return appealOne;
	}

	public void setAppealOne(double appealOne) {
		this.appealOne = appealOne;
	}

	public double getAppealTwo() {
		return appealTwo;
	}

	public void setAppealTwo(double appealTwo) {
		this.appealTwo = appealTwo;
	}

	public double getAppealThree() {
		return appealThree;
	}

	public void setAppealThree(double appealThree) {
		this.appealThree = appealThree;
	}

	public String getTopOne() {
		return topOne;
	}

	public void setTopOne(String topOne) {
		this.topOne = topOne;
	}

	public String getTopTwo() {
		return topTwo;
	}

	public void setTopTwo(String topTwo) {
		this.topTwo = topTwo;
	}

	public String getTopThree() {
		return topThree;
	}

	public void setTopThree(String topThree) {
		this.topThree = topThree;
	}

	public double getComputeColorfulnessx() {
		return computeColorfulnessx;
	}

	public void setComputeColorfulnessx(double computeColorfulnessx) {
		this.computeColorfulnessx = computeColorfulnessx;
	}

	public double getComputeColorfulnessy() {
		return computeColorfulnessy;
	}

	public void setComputeColorfulnessy(double computeColorfulnessy) {
		this.computeColorfulnessy = computeColorfulnessy;
	}

	public int getCountNumberOfLeaves() {
		return countNumberOfLeaves;
	}

	public void setCountNumberOfLeaves(int countNumberOfLeaves) {
		this.countNumberOfLeaves = countNumberOfLeaves;
	}

	public int getCountNumberOfTextGroup() {
		return countNumberOfTextGroup;
	}

	public void setCountNumberOfTextGroup(int countNumberOfTextGroup) {
		this.countNumberOfTextGroup = countNumberOfTextGroup;
	}

	public double getComputeHorizontalBalance() {
		return computeHorizontalBalance;
	}

	public void setComputeHorizontalBalance(double computeHorizontalBalance) {
		this.computeHorizontalBalance = computeHorizontalBalance;
	}

	public double getComputeHorizontalSymmetry() {
		return computeHorizontalSymmetry;
	}

	public void setComputeHorizontalSymmetry(double computeHorizontalSymmetry) {
		this.computeHorizontalSymmetry = computeHorizontalSymmetry;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getCsvPath() {
		return csvPath;
	}

	public void setCsvPath(String csvPath) {
		this.csvPath = csvPath;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
