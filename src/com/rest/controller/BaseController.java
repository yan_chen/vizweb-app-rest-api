package com.rest.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.context.ContextLoader;


@Controller
public class BaseController {

	public static final String IMGROOT = "uploads/";
	
	protected void writeOut(HttpServletResponse response, String msg) {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("UTF-8");
			out = response.getWriter();
			out.write(msg);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
	
	protected String getWebAppPath(){
		String webAppPath = ContextLoader.getCurrentWebApplicationContext().getServletContext().getRealPath("/");
		String userWebAppPath = webAppPath+IMGROOT;
		checkImageDir(userWebAppPath);
		return userWebAppPath;
	}

	protected void checkImageDir(String userWebAppPath) {		
		 File file = new File(userWebAppPath);
		 if(!file.exists()){
			 file.mkdir();
		 }
	}
}
