package com.rest.controller;

import java.awt.image.BufferedImage;



import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.awt.Point;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;


import org.sikuli.design.QuadtreeFeatureComputer;
import org.sikuli.design.XYFeatureComputer;
import org.sikuli.design.color.ColorAnalyzer;
import org.sikuli.design.color.NamedColor;
import org.sikuli.design.color.StandardColors;
import org.sikuli.design.quadtree.ColorEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.IntensityEntropyDecompositionStrategy;
import org.sikuli.design.quadtree.QuadTreeDecomposer;
import org.sikuli.design.quadtree.QuadTreeDecompositionStrategy;
import org.sikuli.design.quadtree.Quadtree;
import org.sikuli.design.structure.Block;
import org.sikuli.design.xycut.DefaultXYDecompositionStrategy;
import org.sikuli.design.xycut.XYDecomposer;
import org.sikuli.design.xycut.XYDecompositionStrategy;
import org.sikuli.design.xycut.XYTextDetector;

import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.rest.bean.ImageList;
import com.rest.bean.ImageMeta;
import java.util.regex.Matcher;

import com.rest.Computation.*;

public class ImageController extends BaseController {

	private ImageList imageList;
	static String csv;
	static int fileSetting;
	
	public static void setAll(){
		
		fileSetting=1;
	}
	public void setImageList(ImageList imageList) {
		this.imageList = imageList;
	}

	private String requestUrl;

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	/**
	 * URL: /service/upload 
	 * supload(): single file upload
	 * @param file
	 *            : MultipartFile
	 * @param response
	 *            : HttpServletResponse auto passed
	 * @return LinkedList<ImageMeta> as json format
	 */
	@RequestMapping(value = "/upload", method = RequestMethod.POST, headers = "Accept=application/json, application/xml")
	public @ResponseBody ImageMeta upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
		Enumeration headerNames = request.getHeaderNames();
		
		while (headerNames.hasMoreElements()) {
		
			String headerName = (String) headerNames.nextElement();
		
			System.out.println(headerName + " : "
					+ request.getHeader(headerName));
	
		}
		ImageMeta meta = null;
		if (!file.isEmpty()) {
			meta = uploadFile(file);
			
			System.out.println("meta success=== "+meta.isSuccess());
		} else {
			meta = new ImageMeta();
			meta.setSuccess(false);
			meta.setMsg("upload failed,can not find the upload file");
		}
		
		return meta;
	}
	
	/**
	 * URL: /service/uploads
	 * uploads(): multiple files upload
	 * @param request
	 *            : MultipartHttpServletRequest auto passed
	 * @param response
	 *            : HttpServletResponse auto passed
	 * @return LinkedList<ImageMeta> as json format
	 */
	@RequestMapping(value = "/uploads", method = RequestMethod.POST, headers = "Accept=application/json, application/xml")
	public @ResponseBody ImageList uploads(MultipartHttpServletRequest request,HttpServletResponse response) throws Exception {
		Iterator<String> itr = request.getFileNames();
		MultipartFile mpf = null;
		LinkedList<ImageMeta> images = new LinkedList<ImageMeta>();
		// get each file
		while (itr.hasNext()) {
			mpf = request.getFile(itr.next());
			ImageMeta imageMeta = uploadFile(mpf);
			images.add(imageMeta);
		}
		imageList.setImages(images);
		imageList.setCount(images.size());
		imageList.setSuccess(true);
		imageList.setMsg("image upload succeed");		
		return imageList;
	}

	/**
	 * URL: /service/capture?url=www.example.com 
	 * capture(): website screenshot
	 * @param response
	 *            : passed by the server
	 * @param url
	 *            : the url of website to capture
	 * @return ImageMeta as json format
	 */
	@RequestMapping(value = "/capture", method = RequestMethod.GET, headers = "Accept=application/json, application/xml")
	public @ResponseBody ImageMeta capture(HttpServletResponse response, @RequestParam String url) throws IOException {
		
		if(url.equals("0"))
		{
			fileSetting=0;	
			return null;
		}
		if(url.equals("1"))
		{
			setAll();
			return null;
		}
		String test = url.substring(url.length()-1, url.length());
		url = url.substring(0, url.length()-1);
		
		
		HttpURLConnection conn = null;
		ImageMeta meta = null;
		
		String userWebAppPath = getWebAppPath();
		try {
			userWebAppPath = userWebAppPath.replace("/", File.separator);
			checkImageDir(userWebAppPath);
			
			
			String imgWebAppPath = userWebAppPath + url + ".jpg";
			File imgFile = new File(imgWebAppPath);
			if (!imgFile.exists()) {
				url = url.replace("+", ".");
				URL uri = new URL(requestUrl + url);
				conn = (HttpURLConnection) uri.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(60 * 1000);
				conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/28.0.1468.0 Safari/537.36

				InputStream is = conn.getInputStream();
				System.out.println(imgWebAppPath);
				OutputStream os = new FileOutputStream(imgFile);
				IOUtils.copy(is, os);
				os.close();
				is.close();
				conn.disconnect();
			}
			BufferedImage buffImg = ImageIO.read(imgFile);
			
			meta = imageProcess.imageProcess(buffImg, imgWebAppPath,test);
			
			meta.setImagePath("../" + IMGROOT + url + ".jpg");
			//check if the csv file has been created yet
			if(test.equals("0"))	
			{
				csv = "../" + IMGROOT + url + ".csv";
				setAll();
				meta.setCsvPath("../" + IMGROOT + url + ".csv");
			}
			else
			{
				setAll();
				meta.setCsvPath(csv);
			}
			
			return meta;
		} catch (Exception e) {
			meta = new ImageMeta();
			meta.setMsg(conn != null ? ("screencapture error,code :" + conn
					.getResponseCode()) : "either the server " + url
					+ " access denied or the path " + userWebAppPath
					+ " is not writable");
			meta.setSuccess(false);
			
			return meta;
		}
	}


	
	/**
	 * URL: /service/screenShot/{url} 
	 * capture(): url screenshot
	 * @param response
	 *            : passed by the server
	 * @param url
	 *            : the website(example:www+google+com) to shot 
	 * @return ImageMeta as json format
	 * @throws IOException
	 */
	@RequestMapping(value = "/screenShot/{url}", method = RequestMethod.GET, headers = "Accept=application/json, application/xml")
	public @ResponseBody ImageMeta screenShot(HttpServletResponse response, @PathVariable String url) throws IOException {
		
		String test="0";
		HttpURLConnection conn = null;
		ImageMeta meta = null;
		String userWebAppPath = getWebAppPath();
		try {
			userWebAppPath = userWebAppPath.replace("/", File.separator);
			checkImageDir(userWebAppPath);
			String imgWebAppPath = userWebAppPath + url + ".jpg";
			File imgFile = new File(imgWebAppPath);
			if (!imgFile.exists()) {
				url = url.replace("+", ".");
				URL uri = new URL(requestUrl + url);
				conn = (HttpURLConnection) uri.openConnection();
				conn.setRequestMethod("GET");
				conn.setConnectTimeout(60 * 1000);
				conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");// Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML,like Gecko) Chrome/28.0.1468.0 Safari/537.36

				InputStream is = conn.getInputStream();
				System.out.println(imgWebAppPath);
				OutputStream os = new FileOutputStream(imgFile);
				IOUtils.copy(is, os);
				os.close();
				is.close();
				conn.disconnect();
			}
			BufferedImage buffImg = ImageIO.read(imgFile);
			meta = imageProcess.imageProcess(buffImg, imgWebAppPath,test);
			meta.setImagePath("../" + IMGROOT + url + ".jpg");
			return meta;
		} catch (Exception e) {
			meta = new ImageMeta();
			meta.setMsg(conn != null ? ("screencapture error,code :" + conn
					.getResponseCode()) : "either the server " + url
					+ " access denied or the path " + userWebAppPath
					+ " is not writable");
			meta.setSuccess(false);
			return meta;
		}
	}

	//create file path
	private String[] createFilePath(String name){
		System.out.println(name);
		String userWebAppPath = getWebAppPath();
		userWebAppPath = userWebAppPath.replace("/", File.separator);

		// file name: timestamp
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss");
		String imgFileId = formatter.format(new Date());
		// file name suffix
		String imgFileExt = name.substring(name.lastIndexOf(".") + 1);
		// create real name of the image
		imgFileId = System.currentTimeMillis() + "." + imgFileExt;

		// file upload path
		String imgWebAppPath = userWebAppPath + imgFileId;
		// file upload relative path
		String imgUploadPath = IMGROOT + imgFileId;
		
		return new String[]{imgWebAppPath,imgUploadPath};
	}
	
	
	private ImageMeta uploadFile(MultipartFile file) {
		//check if the csv file has been created yet
		String test;
		
		if(fileSetting==0)
			test = "0";
		else
			test = "1";
		

		ImageMeta meta = null;
		try {
			String fileName = new String(file.getOriginalFilename().getBytes("ISO8859-1"),"UTF-8");
			String[] pathes = this.createFilePath(fileName);
			String absolutePath = pathes[0];
			String relativePath = pathes[1];
			File uploadFile = new File(absolutePath);
			FileCopyUtils.copy(file.getBytes(), uploadFile);
			meta = imageProcess.imageProcess(ImageIO.read(uploadFile),absolutePath,test);
			meta.setSuccess(true);
			meta.setImagePath(relativePath);
			
			if(test.equals("0"))	
			{
				setAll();
				csv = "../"+ relativePath.substring(0,relativePath.length()-3)+"csv";
				meta.setCsvPath(relativePath.substring(0,relativePath.length()-3)+"csv");
			}
			else
			{
				setAll();
				meta.setCsvPath(csv.substring(3, csv.length()));
			}
		
		} catch (UnsupportedEncodingException ue) {
			meta = new ImageMeta();
			meta.setSuccess(false);
			meta.setMsg("upload failed"+ue.getMessage());
		} catch (IOException e) {
			meta = new ImageMeta();
			meta.setSuccess(false);
			meta.setMsg("upload failed"+e.getMessage());
		}
	
		return meta;
	}
	
	// post file
	public void postFile(File file,String reqUrl) throws Exception {
		HttpClient httpclient = new DefaultHttpClient();
		try {
			// IOUtils.copy(input, output);
			// request execute the url
			HttpPost httppost = new HttpPost(reqUrl);
			// create file
			FileBody fileBody = new FileBody(file);
			// create file form
			StringBody descript = new StringBody("0431.la");

			// filled the requested form
			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("file", fileBody);
			reqEntity.addPart("descript", descript);
			// set entity
			httppost.setEntity(reqEntity);

			System.out.println("Execute request" + httppost.getRequestLine());
			// execute
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity resEntity = response.getEntity();

			System.out.println(response.getStatusLine());
			if (resEntity != null) {
				System.out.println("Response length: " + resEntity.getContentLength());
			}
			EntityUtils.consume(resEntity);
		} finally {
			try {
				httpclient.getConnectionManager().shutdown();
			} catch (Exception ignore) {
			}
		}
	}

	// read the remote html structure of the url
	protected String htmlToString(String webUrl) throws IOException {
		URL url = new URL(webUrl);
		BufferedReader br = new BufferedReader(new InputStreamReader(
				url.openStream()));
		String s = null;
		StringBuffer sb = new StringBuffer();
		while ((s = br.readLine()) != null) {
			sb.append(s);
		}
		br.close();
		System.out.println(sb);
		return sb.toString();
	}

	// transfer stream data to byte data
	protected byte[] readInputStream(InputStream inStream) throws Exception {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		byte[] buffer = new byte[2048];
		int len = 0;
		while ((len = inStream.read(buffer)) != -1) {
			outStream.write(buffer, 0, len);
		}
		return outStream.toByteArray();
	}
}
