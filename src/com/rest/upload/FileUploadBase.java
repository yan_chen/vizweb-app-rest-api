 /*  
  *  Vizweb REST API
  *	 Author: Yan Chen
  *	 Date: 7/1/13 
  */
package com.rest.upload;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

public abstract class FileUploadBase {
	
	//initialization
    protected Map<String, String> parameters = new HashMap<String, String>();
    
    protected String encoding = "UTF-8"; 

    protected UploadFileFilter filter = null; 
    
    protected int sizeThreshold = DiskFileItemFactory.DEFAULT_SIZE_THRESHOLD;

    protected long sizeMax = -1;//infinite

    protected File repository;
    
    public String getParameter(String key){
        return parameters.get(key);
    }

    public String getEncoding(){
        return encoding;
    }

    public void setEncoding(String encoding){
        this.encoding = encoding;
    }

    /** 
     * get the maximum size of the upload file
     * @return
     */
    public long getSizeMax(){
        return sizeMax;
    }

    /**
     * set the maximum valid size of the upload file
     * @param sizeMax
     */
    public void setSizeMax(long sizeMax){
        this.sizeMax = sizeMax;
    }

    /** 
     * get the size of the file space threshold
     */
    public int getSizeThreshold(){
        return sizeThreshold;
    }
    
    /** 
     * set the size of the file space threshold
     */
    public void setSizeThreshold(int sizeThreshold){
        this.sizeThreshold = sizeThreshold;
    }

    /** 
     * get the file repository menu
     */
    public File getRepository() {
        return repository;
    }

    /** 
     * set the file repository menu
     */
    public void setRepository(File repository) {
        this.repository = repository;
    }
    
    /** 
     * get the parameters from the form
     * @return
     */
    public Map<String, String> getParameters() {
        return parameters;
    }

    /**
     * get the upload filter
     * @return
     */
    public UploadFileFilter getFilter() {
        return filter;
    }

    /** 
     * set the upload filter
     * @param filter
     */
    public void setFilter(UploadFileFilter filter) {
        this.filter = filter;
    }
    
    /** 
     * check the if the file is valid
     * @param item
     * @return
     */
    protected boolean isValidFile(FileItem item){
        return !(item == null || item.getName() == "" || item.getSize() == 0 || (filter != null && !filter.accept(item.getName())));
    }
}

