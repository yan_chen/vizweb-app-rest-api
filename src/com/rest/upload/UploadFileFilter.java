package com.rest.upload;

public interface UploadFileFilter{
    /**
     * Identify the uploaded file by the suffix of the file name
     * @param filename file name, exclude the path
     * @return
     */
    public boolean accept(String filename);
}

