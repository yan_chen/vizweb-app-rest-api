package com.rest.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public class ImageCapture {

    private final String FORMATPNG = "png";
    private final String FORMATJPG = "jpg";
    private final String FORMATGIF = "gif";
    
    private final int MINWIDTH = 100;
    private final int MAXWIDTH = 500;
    
    private final int MINHEIGHT = 100;
    private final int MAXHEIGHT = 500;
    
    private final int SCREEN1024 = 1024;
    private final int SCREEN1280 = 1280;
    private final int SCREEN1650 = 1650;
    private final int SCREEN1920 = 1920;
    
    protected String url;
    protected String reqUrl;
    protected String format = FORMATPNG;
    protected int width = MINWIDTH;
    protected int height = MINHEIGHT;
    protected int screen = SCREEN1024;
    protected int timeout = 1200;
    
    /**
     * Initialization
     * @param string url
     */
    public ImageCapture(String reqUrl,String url)
    {
    	this.reqUrl = reqUrl;
        this.url = "http://"+url;
    }
    
    /**
     * get url.
     * @return string
     */
    public String getUrl()
    {
        return this.url;
    }
    
    /**
     * Set width.
     * @param int width
     * @return ImageCapture
     */
    public ImageCapture setWidth(int width)
    {
        this.width = this.minmax((int) width, MINWIDTH, MAXWIDTH);
        return this;
    }
    
    /**
     * Get width.
     * @return int
     */
    public int getWidth()
    {
        return this.width;
    }
    
    /**
     * set height
     * @param int height
     * @return ImageCapture
     */
    public ImageCapture setHeight(int height)
    {
        this.height = this.minmax((int) height, MINHEIGHT, MAXHEIGHT);
        return this;
    }
    
    /**
     * Get height.
     * @return int
     */
    public int getHeight()
    {
        return this.height;
    }
    
    /**
     * Set format type.
     * @param string format
     * @throws RuntimeException
     * @return ImageCapture
     */
    public ImageCapture setFormat(String format)
    {
    	if(format!=null){
    		format = format.toLowerCase();
    		if(format.equals(FORMATPNG) || format.equals(FORMATJPG) || format.equals(FORMATGIF)){
    			this.format = format;
    		}else{
    			throw new RuntimeException("Unsupported file format "+format+"!");
    		}
    	}else{
    		throw new RuntimeException("Unsupported file format "+format+"!");
    	}
        return this;
    }
    
    /**
     * Get format type.
     * @return string
     */
    public String getFormat()
    {
        return this.format;
    }
    
    /**
     * Get screen width.
     * @param string screen
     * @throws RuntimeException
     * @return ImageCapture
     */
    public ImageCapture setScreen(int screen)
    {
        switch (screen) {
            case SCREEN1024:
            case SCREEN1280:
            case SCREEN1650:
            case SCREEN1920:
                this.screen = screen;
                break;
            default:
                throw new RuntimeException("Unsupported screen width "+screen+"!");
        }
        return this;
    }
    
    /**
     * Get screen width.
     * @return int
     */
    public int getScreen()
    {
        return this.screen;
    }
    
    /**
     * Set timeout in seconds.
     * @param int timeout
     * @return ImageCapture
     */
    public ImageCapture setTimeout(int timeout)
    {
        this.timeout = timeout;
        return this;
    }
    
    /**
     * Get timeout.
     * @return int
     */
    public int getTimeout()
    {
        return this.timeout;
    }
    
    /**
     * Get capture url.
     * @return string
     */
    public String getCaptureUrl()
    {
        return reqUrl + 
            "?width="   + this.width +
            "&height="  + this.height +
            "&format="  + this.format +
            "&screen="  + this.screen +
            "&url=" + url;
    }
    
    /**
     * Execute the capture call.
     * @return HttpRequest
     */
    public HttpRequest callCapture()
    {
        return new HttpRequest(this.getCaptureUrl());
    }
    
    /**
     * Execute the get-status call.
     * @return HttpRequest
     */
    public HttpRequest callGetStatus()
    {
        return new HttpRequest(this.getCaptureUrl(),"&action=get-status");
    }
    
    /**
     * Check if the screenshot has been captured
     * @return boolean
     */
    public boolean isCaptured()
    {
        HttpRequest call = this.callGetStatus();
        String response = call.getResponse();
        if(response.equals("finished")){
        	return true;
        }else if(response.equals("waiting") || response.equals("pending") || response.equals("loaded")){
        	return false;
        }else{
        	throw new RuntimeException("isCaptured crashed!");
        }
    }
    
    public HttpRequest capture(boolean wait) throws InterruptedException
    {
        int i = 0;
        this.callCapture();
        if (wait) {
            while (!this.isCaptured()) {
                if (i++ >= this.timeout) {
                    throw new RuntimeException("Time out, please try it again!");
                }
                Thread.sleep(1);
            }
        }
        return this.callCapture();
    }
    
    public HttpRequest captureToOutput(HttpServletResponse response, boolean wait) throws Exception
    {
    	HttpRequest call = this.capture(wait);
    	
    	response.setContentType(call.getContentType());// set the format of the return file
    	response.setContentLength(call.getContentLength());
    	response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        //response.setContentType("image/jpg"); 
        
        InputStream inStream = call.getInputStream();// get data from the input stream
		BufferedImage buffImg = ImageIO.read(inStream);
        // set the output image to servlet output stream
        ServletOutputStream sos = response.getOutputStream();
        ImageIO.write(buffImg, "jpeg", sos);
        
        inStream.close();
        sos.flush();
        sos.close();
        System.out.println(call.getResponse());
        return call;
    }
    
    /**
     * Save the captured screenshot to local driver
     * @param string fileName
     * @throws RuntimeException
     * @return HttpRequest
     * @throws InterruptedException 
     */
    public HttpRequest captureToFile(String fileName, boolean wait) throws Exception
    {
    	HttpRequest call = this.capture(wait);
    	InputStream ins = call.getInputStream();
    	FileOutputStream fos = new FileOutputStream(new File(fileName));
    	IOUtils.copy(ins, fos);
    	fos.close();
    	ins.close();
        return call;
    }
    
    protected int minmax(int x, int min, int max)
    {
        if (x < min) {
            return min;
        } else if (x > max) {
            return max;
        }
        return x;
    }
}

class HttpRequest
{
    
    private String response;
    private InputStream inputStream;
    private int statusCode;
    private String contentType;
    private int contentLength;
    
    /**
     * Create and execute http request
     * @param url request uri
     * @throws RuntimeException
     */
    public HttpRequest(String uri)
    {
    	try {
    		URL url = new URL(uri);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.setConnectTimeout(60 * 1000);
    		conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");//Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36
      
            this.inputStream = conn.getInputStream();
            if (this.inputStream == null) {
                throw new RuntimeException("Execute failed!");
            }
            
            this.statusCode = conn.getResponseCode();
            this.contentType = conn.getContentType();
            this.contentLength = conn.getContentLength();
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
    }
    
    public HttpRequest(String uri,String actionParams)
    {
    	try {
    		URL url = new URL(uri+actionParams);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setRequestMethod("GET");
    		conn.setConnectTimeout(60 * 1000);
    		conn.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");//Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36
    		InputStream inStream = conn.getInputStream();// get data from the output stream
            this.response = streamToString(inStream);
            System.out.println(this.response);
            if (this.response == null) {
                throw new RuntimeException("catpure failed");
            }
            this.statusCode = conn.getResponseCode();
            this.contentType = conn.getContentType();
            this.contentLength = conn.getContentLength();
            inStream.close();
		} catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
    }
    
    // transfer the stream to string type
    public String streamToString(InputStream in) throws IOException
    {
        BufferedReader breader = null;
        try {
        	
        	breader = new BufferedReader(new InputStreamReader(in, "utf-8"));
        } catch (UnsupportedEncodingException e) {
        }
        StringBuilder builder = new StringBuilder();
        String line;    
        
        // For every line in the file, append it to the string builder
        while((line = breader.readLine()) != null)
        {
            builder.append(line);
        }

        return builder.toString();
    }

    public String getResponse()
    {
        return this.response;
    }
    
    public InputStream getInputStream()
    {
        return this.inputStream;
    }
    
    /**
     * Get http status code.
     * @return int
     */
    public int getStatusCode()
    {
        return this.statusCode;
    }
    
    /**
     * Get content type.
     * @return string
     */
    public String getContentType()
    {
        return this.contentType;
    }
    
    /**
     * Get content length.
     * @return int
     */
    public int getContentLength()
    {
        return this.contentLength;
    }
    
}
